﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace geducSite.Models
{
    public class Contato
    {
        /// <summary>
        /// atributos da classe contato
        /// </summary>
        public int idContato { get; set; }
        public string telefoneFixo { get; set; }
        public string telefoneCelular { get; set; }
        public string email { get; set; }
        public string outros { get; set; }

        /// <summary>
        /// estou modelando a classe pesso dentro da minha classe contato dizendo que contato possui uma pessoa 
        /// </summary>
        public Pessoa pessoa { get; set; }



    }
}
