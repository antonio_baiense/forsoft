﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Models;

namespace geducSite.Models
{
    public class Nota
    {
        public int idNota { get; set; }
        public float nota { get; set; }
        public string periodo { get; set; }
        public string origem { get; set; }
        public DateTime data { get; set; }


        public Aluno aluno { get; set; }
        public Disciplina disciplina { get; set; }
    
    }
}