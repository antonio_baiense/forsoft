﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using geducSite.Models;

namespace geducSite.Models
{
    public class Aluno : Pessoa
    {
        /// <summary>
        /// A classe aluno estar herdado a classe pessoa e outras classes que estão se relacionado com pessoa
        /// </summary>
        public int idAluno { get; set; }
        public string situacao { get; set; }
        public string matricula { get; set; }

        public Curso curso { get; set; }
        public Nota nota { get; set; }
        
    }
}