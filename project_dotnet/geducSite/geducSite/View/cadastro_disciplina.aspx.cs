﻿using geducSite.Models;
using geducSite.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace geducSite.View
{
    public partial class cadastro_disciplina : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void btnCadastrar_Click(object sender, EventArgs e)
        {
            msg.InnerText = "";
            String[] campos = new String[] { txtNomeDisciplina.Text, txtCodigoDisciplina.Text, txtCargaHorariaDisciplina.Text };
            if (!Validador.seAlgumVazio(campos))
            {
                if (Validar.seSomenteNumero(txtCargaHorariaDisciplina.Text)) {
                    Disciplina dis = new Disciplina();
                    dis.nome = txtNomeDisciplina.Text;
                    dis.codigo = txtCodigoDisciplina.Text;
                    dis.cargaHoraria = Convert.ToInt32(txtCargaHorariaDisciplina.Text);

                    new DisciplinaDAO().Cadastrar(dis);

                    msg.InnerText = "Cadastrado com sucesso!!!";
                    limpar();
                }
                else msg.InnerText = "Preenchimento de campo inválido";
            }
            else
            {
                msg.InnerText = "Campo não preenchido";
            }
        }

        public void limpar()
        {
            txtNomeDisciplina.Text = string.Empty;
            txtCodigoDisciplina.Text = string.Empty;
            txtCargaHorariaDisciplina.Text = string.Empty;
        }
    }
}